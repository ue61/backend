package ru.bober.service.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.bober.service.entity.User;
import ru.bober.service.repository.RegistrationRepository;

import java.util.HashSet;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private RegistrationRepository userRepository;

    public List<User> getAll() {
        return this.userRepository.findAll();
    }

    public User getByLogin(String login) {
        return this.userRepository.findByName(login);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User u = getByLogin(username);
        if(u == null) throw new UsernameNotFoundException("Could not find user");
        return new org.springframework.security.core.userdetails.User(u.getName(), u.getPassword(), true, true, true, true, new HashSet<>());
    }
}
