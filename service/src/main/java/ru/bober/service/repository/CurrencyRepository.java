package ru.bober.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bober.service.entity.ValutesCurrency;

@Repository
public interface CurrencyRepository extends JpaRepository<ValutesCurrency, Integer> {
    ValutesCurrency findByValutename(String valutename);
}