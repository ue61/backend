package ru.bober.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bober.service.entity.Role;

public interface RoleRepository extends JpaRepository<Role,Integer> {
}
