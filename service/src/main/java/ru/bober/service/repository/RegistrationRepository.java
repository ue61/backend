package ru.bober.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bober.service.entity.User;

@Repository
public interface RegistrationRepository extends JpaRepository<User , Long> {
    User findByName(String username);
}
