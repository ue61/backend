package ru.bober.service.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.bober.service.entity.History;
import ru.bober.service.entity.User;
import ru.bober.service.entity.ValutesCurrency;
import ru.bober.service.repository.CurrencyRepository;
import ru.bober.service.repository.HistoryRepository;
import ru.bober.service.repository.RegistrationRepository;

import java.sql.Date;
import java.util.List;
import java.util.UUID;

@RestController
public class IndexController {

    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private RegistrationRepository registrationRepository;

    @GetMapping(path="/api/histories", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody Iterable<History> getProducts(){
        Gson gson = new Gson();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userId = registrationRepository.findByName(auth.getName()).getId();
        return historyRepository.findAllByUserId(userId);
    }

    @GetMapping("/api/currencies")
    public List<ValutesCurrency> getCurrencies(){
        return currencyRepository.findAll();
    }
}
