package ru.bober.service.controller;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.bober.service.entity.User;
import ru.bober.service.repository.RegistrationRepository;

import java.util.UUID;

@RestController
public class RegistrationController {
    @Autowired
    private RegistrationRepository userRepository;

    @ResponseBody
    @PostMapping(path="/api/registration", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> registration(@RequestBody User user){
        if(userRepository.findByName(user.getName()) != null){
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
        UUID uuid = UUID.randomUUID();
        user.setId(uuid.toString());
        userRepository.save(user);
        return new ResponseEntity<String>(HttpStatus.OK);
    }
}