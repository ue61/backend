package ru.bober.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.bober.service.entity.User;
import ru.bober.service.repository.RegistrationRepository;

import java.util.Objects;

@RestController
public class AuthController {
    @Autowired
    private RegistrationRepository userRepository;

    @PostMapping(path = "/api/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody User getAuthUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            System.out.println("ya tyt");
            return null;
        }
        User users = userRepository.findByName(auth.getName());
        return users;
    }
}