package ru.bober.web.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Entity
@Table(name="user_boberprj")
public class User
{
    @Id
    private String id;
    @Column(nullable=false)
    @NotBlank(message = "Username cannot be empty")
    private String name;
    @Column(nullable=false, unique=true)
    @Email(message = "Incorrect email pattern")
    @NotBlank(message = "Email cannot be empty")
    private String email;
    @Column(nullable=false)
    @Size(min=4)
    @NotBlank(message = "Password cannot be empty")
    private String password;



    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name="user_role_boberprj",
            joinColumns={@JoinColumn(name="USER_ID", referencedColumnName="ID")},
            inverseJoinColumns={@JoinColumn(name="ROLE_ID", referencedColumnName="ID")})
    private Set<Role> roles;

    public User() {
    }

    public User(String id, String name, String email, String password, Set<Role> roles) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getEmail()
    {
        return email;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }
    public String getPassword()
    {
        return password;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }
    public Set<Role> getRoles()
    {
        return roles;
    }
    public void setRoles(Set<Role> roles)
    {
        this.roles = roles;
    }
}
