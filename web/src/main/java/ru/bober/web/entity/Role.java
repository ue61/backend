package ru.bober.web.entity;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "role_boberprj")
public class Role {
    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(nullable = false, unique = true)

    private String name;

    @ManyToMany(mappedBy = "roles")
    private Set < User > users;

    public Role() {
    }

    public Role(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set< User > getUsers() {
        return users;
    }

    public void setUsers(Set < User > users) {
        this.users = users;
    }
}
