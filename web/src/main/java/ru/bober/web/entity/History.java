package ru.bober.web.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.Calendar;


@Entity
@Data
@Table(name = "history")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;
    public History() {
    }
    public History(int id, String userId, String inValute, String outValute, int inValue, int outValue, float currency) {
        this.id = id;
        this.userId = userId;
        this.inValute = inValute;
        this.outValute = outValute;
        this.inValue = inValue;
        this.outValue = outValue;
        this.currency = currency;
        this.date = new Date(Calendar.DATE);
    }

    @Column(name = "user_id")
    private String userId;
    @Column (name = "in_valute")
    private String inValute;
    @Column (name = "out_valute")
    private String outValute;
    @Column (name = "in_value")
    private int inValue;
    @Column (name = "out_value")
    private int outValue;
    @Column (name = "currency")
    private float currency;
    @Column (name = "date")
    private Date date;

}
