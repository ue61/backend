package ru.bober.web.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "account")
public class Account{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private int id;

    @Column(name = "user_id")
    private String userId;

    private float AUD = 0.0f;
    private float AZN = 0.0f;
    private float GBP = 0.0f;
    private float AMD = 0.0f;
    private float BYN = 0.0f;
    private float BGN = 0.0f;
    private float BRL = 0.0f;
    private float HUF = 0.0f;
    private float HKD = 0.0f;
    private float DKK = 0.0f;
    private float USD = 0.0f;
    private float EUR = 0.0f;
    private float INR = 0.0f;
    private float KZT = 0.0f;
    private float CAD = 0.0f;
    private float KGS = 0.0f;
    private float CNY = 0.0f;
    private float MDL = 0.0f;
    private float NOK = 0.0f;
    private float PLN = 0.0f;
    private float RON = 0.0f;
    private float XDR = 0.0f;
    private float SGD = 0.0f;
    private float TJS = 0.0f;
    private float TRY = 0.0f;
    private float TMT = 0.0f;
    private float UZS = 0.0f;
    private float UAH = 0.0f;
    private float CZK = 0.0f;
    private float SEK = 0.0f;
    private float CHF = 0.0f;
    private float ZAR = 0.0f;
    private float KRW = 0.0f;
    private float JPY = 0.0f;
    private float RUB = 0.0f;
}
