package ru.bober.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.bober.web.entity.Account;
import ru.bober.web.entity.History;
import ru.bober.web.entity.User;
import ru.bober.web.entity.ValutesCurrency;
import ru.bober.web.repository.HistoryRepository;
import ru.bober.web.repository.RegistrationRepository;
import ru.bober.web.service.HistoryService;
import ru.bober.web.service.TransactionService;
import ru.bober.web.service.WebService;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
public class HistoryController {
    @Autowired
    HistoryService historyService;
    @Autowired
    HistoryRepository historyRepository;
    @Autowired
    RegistrationRepository registrationRepository;
    @Autowired
    WebService webService;

    @ResponseBody
    @GetMapping("/api/histories")
    public Iterable<History> getApiProducts(@RequestHeader HttpHeaders headers){
        return webService.getHistory(headers);
    }
    @GetMapping("/touserhistory")
    public String toUserHistory(Model model){
        model.addAttribute("users", registrationRepository.findAll());
        return "adminpage";
    }
    @GetMapping("/historyofuser")
    public String historyOfUser(@RequestParam String name, Model model){
        User user = registrationRepository.findByName(name);
        model.addAttribute("users", registrationRepository.findAll());
        model.addAttribute("histories", historyRepository.findAllByUserId(user.getId()));
        return "adminpage";
    }

    @GetMapping("/history")
    public String toHistory(Model model){
        model.addAttribute("filtered", historyRepository.findAllByUserId(registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName()).getId()));
        model.addAttribute("filters", historyRepository.findAllByUserId(registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName()).getId()));
        return "history";
    }
    @GetMapping("/filteredHistory")
    public String filteredHistory(@RequestParam String in_valute, @RequestParam String out_valute, @RequestParam String dateStr, Model model) throws ParseException {
        String buf = registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName()).getId();
        SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date1;
        Date date;
        if (dateStr == ""){
            date = null;
        }
        else {
            date1 = sdfo.parse(dateStr);
            date = new Date(date1.getTime());
        }
        model.addAttribute("filters", historyRepository.findAll());
        if (date == null && in_valute == "" && out_valute == ""){
            model.addAttribute("filtered", historyRepository.findAllByUserId(buf));
        }
        else if (date != null && in_valute == "" && out_valute == ""){
            model.addAttribute("filtered", historyRepository.findAllByDateAndUserId(date,buf));
        }
        else if (date == null && in_valute != "" && out_valute == ""){
            model.addAttribute("filtered", historyRepository.findAllByInValuteAndUserId(in_valute,buf));
        }
        else if (date == null && in_valute == "" && out_valute != ""){
            model.addAttribute("filtered", historyRepository.findAllByOutValuteAndUserId(out_valute,buf));
        }
        else if (date != null && in_valute != "" && out_valute == ""){
            model.addAttribute("filtered", historyRepository.findAllByDateAndInValuteAndUserId(date, in_valute,buf));
        }
        else if (date != null && in_valute == "" && out_valute != ""){
            model.addAttribute("filtered", historyRepository.findAllByDateAndOutValuteAndUserId(date, out_valute,buf));
        }
        else if (date == null && in_valute != "" && out_valute != ""){
            model.addAttribute("filtered", historyRepository.findAllByInValuteAndOutValuteAndUserId(in_valute, out_valute,buf));
        }
        else if (date != null && in_valute != "" && out_valute != ""){
            model.addAttribute("filtered", historyRepository.findAllByDateAndInValuteAndOutValuteAndUserId(date, in_valute, out_valute,buf));
        }
        return "history";
    }
}
