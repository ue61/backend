package ru.bober.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.bober.web.entity.Account;
import ru.bober.web.entity.User;
import ru.bober.web.repository.AccountRepository;
import ru.bober.web.repository.CurrencyRepository;
import ru.bober.web.repository.RegistrationRepository;
import ru.bober.web.service.CurrencyService;
import ru.bober.web.service.TransactionService;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TransactionController {
    @Autowired
    RegistrationRepository registrationRepository;
    @Autowired
    TransactionService transactionService;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    CurrencyService currencyService;
    @Autowired
    CurrencyRepository currencyRepository;

    @GetMapping("/totransactionpage")
    public String toTransactionPage(Model model){
        model.addAttribute("users", registrationRepository.findAll());
        model.addAttribute("names", currencyService.getValutesNames());
        return "transactionpage";
    }
    @PostMapping("/transactiondone")
    public String doTransaction(@RequestParam String name, @RequestParam float num, @RequestParam String valName, Model model) throws IllegalAccessException {
        User user = registrationRepository.findByName(name);
        Account account2 = accountRepository.findAccountByUserId(user.getId());
        Account account1 = accountRepository.findAccountByUserId(registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName()).getId());
        transactionService.makeTransaction(account1,account2,num, currencyRepository.findByValutename(valName).getChar_code());
        model.addAttribute("users", registrationRepository.findAll());
        model.addAttribute("names", currencyService.getValutesNames());
        return "transactionpage";
    }

    @GetMapping("/toaddingpage")
    public String toAddingPage(Model model){
        model.addAttribute("users", registrationRepository.findAll());
        model.addAttribute("names", currencyService.getValutesNames());
        return "addingpage";
    }
    @PostMapping("/addsum")
    public String addSum(@RequestParam float num, @RequestParam String valName, Model model) throws IllegalAccessException {
        Account account1 = accountRepository.findAccountByUserId(registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName()).getId());
        transactionService.addCurrencySum(account1,num, currencyRepository.findByValutename(valName).getChar_code());
        model.addAttribute("names", currencyService.getValutesNames());
        return "addingpage";
    }

    @GetMapping("/toaccountpage")
    public String toAccountPage(Model model) throws IllegalAccessException {
        Account account = accountRepository.findAccountByUserId(registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName()).getId());
        Field[] fields = account.getClass().getDeclaredFields();
        Map<String,Float> map = new HashMap<>();
        for (Field field : fields) {
            if(field.getName()!="id" && field.getName()!="userId") {
                field.setAccessible(true);
                map.put(field.getName(), field.getFloat(account));
                field.setAccessible(false);
            }
        }
        model.addAttribute("fields", map);
        return "accountpage";
    }
}