package ru.bober.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.bober.web.service.WebService;
import ru.bober.web.entity.ValutesCurrency;
import ru.bober.web.repository.CurrencyRepository;
import ru.bober.web.repository.RegistrationRepository;
import ru.bober.web.service.CurrencyService;
import ru.bober.web.service.HistoryService;

import java.util.List;

@Controller
public class CurrencyController {
    @Autowired
    CurrencyService currencyService;
    @Autowired
    CurrencyRepository currencyRepository;
    @Autowired
    RegistrationRepository registrationRepository;
    @Autowired
    HistoryService historyService;
    @Autowired
    WebService webService;

    @GetMapping("/converter")
    public String calc(Model model){
        currencyService.getValutes();
        model.addAttribute("first",currencyRepository.findByValutename("Австралийский доллар"));
        model.addAttribute("second",currencyRepository.findByValutename("Австралийский доллар"));
        model.addAttribute("names", currencyService.getValutesNames());
        return "converter";
    }
    @ResponseBody
    @GetMapping("/api/currencies")
    public List<ValutesCurrency> getApiCurrencies(){
        return webService.getCurrency();
    }
    @PostMapping("/toconvert")
    public String convert(@RequestParam String vn1, @RequestParam String vn2, @RequestParam String num, Model model) throws IllegalAccessException {
        if (vn1==null && currencyService.getMementoValuteNameOne()==null){
            vn1 = currencyRepository.findByValutename("Австралийский доллар").getValutename();
            currencyService.setMementoValuteNameOne(vn1);
        }
        else if (vn1==null && currencyService.getMementoValuteNameOne()!=null) {
            vn1 = currencyService.getMementoValuteNameOne();
        }
        else {
            currencyService.setMementoValuteNameOne(vn1);
        }
        if (vn2==null && currencyService.getMementoValuteNameTwo()==null){
            vn2 = currencyRepository.findByValutename("Австралийский доллар").getValutename();
            currencyService.setMementoValuteNameTwo(vn2);
        }
        else if (vn2==null && currencyService.getMementoValuteNameTwo()!=null) {
            vn2 = currencyService.getMementoValuteNameTwo();
        }
        else {
            currencyService.setMementoValuteNameTwo(vn2);
        }
        model.addAttribute("first",currencyRepository.findByValutename(vn1));
        model.addAttribute("second",currencyRepository.findByValutename(vn2));
        if (num == "" && currencyService.getMementoNum()==null){
            model.addAttribute("answer", "Вы не ввели, сколько нужно перевести");
        }
        else if(num == "" && currencyService.getMementoNum()!=null){
                float buf = Float.parseFloat(currencyService.getMementoNum());
                model.addAttribute("number", currencyService.getMementoNum());
                model.addAttribute("answer", currencyService.toConvertUserSum(vn1, vn2, buf, registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName())));
        }
        else {
            try {
                Float.parseFloat(num);
            } catch (NumberFormatException e){
                num= "error";
                model.addAttribute("answer", "Вы ввели не число");
            }
            if(num != "error") {
                currencyService.setMementoNum(num);
                float buf = Float.parseFloat(num);
                model.addAttribute("number", num);
                model.addAttribute("answer", currencyService.toConvertUserSum(vn1, vn2, buf, registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName())));
                historyService.addRecord(registrationRepository.findByName(SecurityContextHolder.getContext().getAuthentication().getName()).getId(),vn1,vn2);
            }
        }
        model.addAttribute("names", currencyService.getValutesNames());
        return "converter";
    }
}
