package ru.bober.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.bober.web.entity.User;
import ru.bober.web.service.RegistrationService;
import ru.bober.web.service.WebService;

@Controller
public class RegistrationController {
    @Autowired
    RegistrationService registrationService;
    @Autowired
    WebService webService;

    @PostMapping(path = "/registration")
    public String addUser(@Validated User user, Model model) {
        if (registrationService.findOneUserByLogin(user.getName())) {
            model.addAttribute("UserAlreadyExist", "Пользователь с таким логином уже существует.");
            return "registration";
        } else {
            registrationService.createUser(user, "ROLE_USER");
            return "home";
        }
    }
    @ResponseBody
    @PostMapping("/api/registration")
    public ResponseEntity<String> registrationApi(@RequestBody User users){
        return webService.register(users);
    }

    @GetMapping("/login")
    public String authorizationPage() {
        return "login";
    }

    @GetMapping("/register")
    public String addUser() {
        return "registration";
    }
}