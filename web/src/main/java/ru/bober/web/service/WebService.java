package ru.bober.web.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import ru.bober.web.entity.History;
import ru.bober.web.entity.User;
import ru.bober.web.entity.ValutesCurrency;

import java.util.List;

@Service
public class WebService {

    private final RestTemplate template;
    private String serverURL = "http://localhost:8082";

    public WebService(RestTemplate template){
        this.template = template;
    }

    public List<ValutesCurrency> getCurrency() {
        return template.getForObject(serverURL + "/api/currencies", List.class);
    }

    public Iterable<History> getHistory(HttpHeaders header){
        HttpEntity entity = new HttpEntity(header);
        ResponseEntity<Iterable<History>> temp = template.exchange(serverURL + "/api/histories",HttpMethod.GET, entity, new ParameterizedTypeReference<Iterable<History>>() {});
       Iterable<History> result = temp.getBody();
        return result;
    }
    public ResponseEntity<String> register(User user){
        ResponseEntity<String> result = template.postForEntity(serverURL + "/api/registration", user, String.class);
        return result;
    }

    public ResponseEntity<User> login(HttpHeaders header){
        HttpEntity entity = new HttpEntity(header);
        ResponseEntity<User> result = template.exchange(serverURL + "/api/login", HttpMethod.POST, entity, User.class);
        return result;
    }
}