package ru.bober.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bober.web.entity.History;
import ru.bober.web.repository.CurrencyRepository;
import ru.bober.web.repository.HistoryRepository;

import java.sql.Date;


@Service
public class HistoryService {
    @Autowired
    HistoryRepository historyRepository;
    @Autowired
    CurrencyRepository currencyRepository;

    public void addRecord(String id,String vn1, String vn2){
        History history = new History();
        Date date = new Date(new java.util.Date().getTime());
        history.setInValue(currencyRepository.findByValutename(vn1).getValue());
        history.setOutValue(currencyRepository.findByValutename(vn2).getValue());
        history.setDate(date);
        history.setUserId(id);
        history.setInValute(currencyRepository.findByValutename(vn1).getChar_code() + " (" + vn1 + ")");
        history.setOutValute(currencyRepository.findByValutename(vn2).getChar_code() + " (" + vn2 + ")");
        history.setCurrency((float) currencyRepository.findByValutename(vn1).getValue() / (float)currencyRepository.findByValutename(vn2).getValue());
        historyRepository.save(history);
    }
}
