package ru.bober.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bober.web.entity.Account;
import ru.bober.web.repository.AccountRepository;

import javax.transaction.Transactional;
import java.lang.reflect.Field;

@Service
public class TransactionService {
    @Autowired
    AccountRepository accountRepository;

    @Transactional
    public void makeTransaction(Account account1, Account account2, float sum, String code) throws IllegalAccessException {
        Field[] fields = account1.getClass().getDeclaredFields();
        if (sum > 0) {
            for (Field field : fields) {
                if (field.getName().equals(code)) {
                    field.setAccessible(true);
                    if (field.getFloat(account1) - sum >= 0.0f) {
                        field.set(account1, field.getFloat(account1) - sum);
                        field.set(account2, field.getFloat(account2) + sum);
                    }
                    field.setAccessible(false);
                    break;
                }
            }
        }
    }

    @Transactional
    public void addCurrencySum(Account account, float sum, String code) throws IllegalAccessException {
        Field[] fields = account.getClass().getDeclaredFields();
        if (sum > 0) {
            for (Field field : fields) {
                if (field.getName().equals(code)) {
                    field.setAccessible(true);
                    field.set(account, field.getFloat(account) + sum);
                    field.setAccessible(false);
                    break;
                }
            }
        }
    }
}
