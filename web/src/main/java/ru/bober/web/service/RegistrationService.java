package ru.bober.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.bober.web.entity.Account;
import ru.bober.web.entity.Role;
import ru.bober.web.entity.User;
import ru.bober.web.repository.AccountRepository;
import ru.bober.web.repository.RegistrationRepository;
import ru.bober.web.repository.RoleRepository;

import javax.transaction.Transaction;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
public class RegistrationService {
    @Autowired
    RegistrationRepository registrationRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    AccountRepository accountRepository;

    public void createUser(User user,  String role) {
        UUID uuid = UUID.randomUUID();
        List<Role> roles = roleRepository.findAll();
        Account account = new Account();
        for (Role r:roles) {
            if(r.getName().equals(role)){
                user.setRoles(Collections.singleton(r));
            }
        }
        user.setId(uuid.toString());
        account.setUserId(user.getId());
        accountRepository.save(account);
        registrationRepository.save(user);
    }
    public boolean findOneUserByLogin(String login){
        if(registrationRepository.findByName(login)!=null)
            return true;
        else return false;
    }
}
