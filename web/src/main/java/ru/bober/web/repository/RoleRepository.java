package ru.bober.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.bober.web.entity.Role;

public interface RoleRepository extends JpaRepository<Role,Integer> {
}
