package ru.bober.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bober.web.entity.History;

import java.sql.Date;

@Repository
public interface HistoryRepository extends JpaRepository<History, Integer> {
    Iterable<History> findAllByUserId(String userId);
    Iterable<History> findAllByInValuteAndOutValuteAndUserId(String in_valute, String out_valute, String userId);
    Iterable<History> findAllByInValuteAndUserId(String in_valute, String userId);
    Iterable<History> findAllByOutValuteAndUserId(String out_valute, String userId);
    Iterable<History> findAllByDateAndUserId(Date date, String userId);
    Iterable<History> findAllByDateAndInValuteAndUserId(Date date, String in_valute, String userId);
    Iterable<History> findAllByDateAndOutValuteAndUserId(Date date, String out_valute, String userId);
    Iterable<History> findAllByDateAndInValuteAndOutValuteAndUserId(Date date, String in_valute, String out_valute, String userId);
}
