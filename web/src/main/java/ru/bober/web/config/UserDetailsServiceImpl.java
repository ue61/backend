package ru.bober.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.*;
import ru.bober.web.entity.User;
import ru.bober.web.repository.RegistrationRepository;

public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private RegistrationRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user = userRepository.findByName(username);

        if (user == null) {
            throw new UsernameNotFoundException("Could not find user");
        }

        return new MyUserDetails(user);
    }
}
