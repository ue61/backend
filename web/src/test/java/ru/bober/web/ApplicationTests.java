package ru.bober.web;

import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.bober.web.repository.HistoryRepository;
import ru.bober.web.repository.RegistrationRepository;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@SpringBootTest
public class ApplicationTests {
    @MockBean
    private RegistrationRepository userRepository;

    @MockBean
    private HistoryRepository historyRepository;

    private static TestService testService;
    @Autowired
    WebApplicationContext wac;

    private MockMvc mockMvc;

    @BeforeAll
    public static void init(){
        testService = new TestService();
    }

    @Before
    public void setup(){
        mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).dispatchOptions(true).build();
    }

    @Test
    public void saveTest(){
        when(userRepository.findByName(eq("qwe"))).thenReturn(testService.getUsers());
        testService.saveUsers(userRepository);
        verify(userRepository).save(testService.getUsers());

        testService.saveHistory(historyRepository);
        verify(historyRepository).save(testService.getHistory());
    }

    @Test
    public void mvcTest(){
        try {
            ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/history"));

            result.andExpect(MockMvcResultMatchers.view().name("history"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
