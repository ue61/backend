package ru.bober.web;
import org.springframework.stereotype.Service;
import ru.bober.web.entity.*;
import ru.bober.web.repository.HistoryRepository;
import ru.bober.web.repository.RegistrationRepository;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.sql.Date;


public class TestService {
    private static User user = new User("c595f739-3375-4833-b400-91533ef26949",
            "qwerty",
            "$2a$08$Gias0ABuaYI3K//XhA2VZOXGnnsosOR/UqJ2052ADn0XwMoLWVl72",
            "bober@gmail.ru",
            Collections.singleton(new Role(1, "ADMIN")));


    private static History history = new History(1,"b8646948-ac4a-446c-842e-a9ea62a4bcca0","AUD (Австралийский доллар)","USD (Доллар США)",548343,721694,0.75979984f);


    public User getUsers(){
        return user;
    }
    public void saveUsers(RegistrationRepository repository){
        repository.save(this.getUsers());
    }


    public History getHistory(){
        return history;
    }

    public void saveHistory(HistoryRepository repository){
        repository.save(this.getHistory());
    }
}

