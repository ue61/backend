package ru.bober.web;

import org.junit.Before;
import org.junit.Assert;

import org.junit.jupiter.api.BeforeAll;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ru.bober.web.repository.RegistrationRepository;


import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestContainer {
    private static PostgreSQLContainer pg;
    @Autowired
    private RegistrationRepository userRepository;

    private static TestService testService;

    @Before
    public void init(){
        testService = new TestService();
    }

    @BeforeAll
    public static void setUp(){
        pg = new PostgreSQLContainer("postgres");
        pg.start();
    }

    @Test
    @Transactional
    public void dbTest(){
        insertUsers();
        Assert.assertEquals(userRepository.findByName("qwerty").getName(), testService.getUsers().getName());
    }

    private void insertUsers(){
        testService.saveUsers(userRepository);
    }
}